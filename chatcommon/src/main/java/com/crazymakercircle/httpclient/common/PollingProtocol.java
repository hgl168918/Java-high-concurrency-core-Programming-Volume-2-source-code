package com.crazymakercircle.httpclient.common;

public interface PollingProtocol<REQ> {

    REQ createExecRequest(String service,
                          Object msg,
                          PollingOptions options) throws ProtocolException;



}
