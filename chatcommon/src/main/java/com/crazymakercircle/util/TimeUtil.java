package com.crazymakercircle.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public final class TimeUtil {

    private static volatile long currentTimeMillis;
    private static SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat("yyyy-MM-dd-HH时mm分ss秒");
    private static SimpleDateFormat TIME_FORMATTER = new SimpleDateFormat("HH:mm:ss");
    static {
        currentTimeMillis = System.currentTimeMillis();
        Thread daemon = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    currentTimeMillis = System.currentTimeMillis();
                    try {
                        TimeUnit.MILLISECONDS.sleep(1);
                    } catch (Throwable e) {

                    }
                }
            }
        });
        daemon.setDaemon(true);
        daemon.setName("time-tick-thread");
        daemon.start();
    }

    public static long currentTimeMillis() {
        return currentTimeMillis;
    }
    public static String currentTime() {

        Date date = new Date(currentTimeMillis);
        return TIME_FORMATTER.format(date);
    }

    public static String currentData() {

        Date date = new Date(currentTimeMillis);
        return DATE_FORMATTER.format(date);
    }
}
