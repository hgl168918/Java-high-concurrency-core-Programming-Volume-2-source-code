package com.crazymakercircle.disruptor;

import com.crazymakercircle.util.Print;
import com.crazymakercircle.util.ThreadUtil;
import com.lmax.disruptor.EventTranslatorOneArg;
import com.lmax.disruptor.RingBuffer;

class LongEventMultiProducerWithTranslator extends Thread {
    //一个translator可以看做一个事件初始化器，publicEvent方法会调用它
    //填充Event
    private static final EventTranslatorOneArg<LongEvent, Long> TRANSLATOR =
            new EventTranslatorOneArg<LongEvent, Long>() {
                public void translateTo(LongEvent event, long sequence, Long data) {
                    event.setValue(data);
                }
            };
    private final RingBuffer<LongEvent> ringBuffer;
    public LongEventMultiProducerWithTranslator(RingBuffer<LongEvent> ringBuffer) {
        this.ringBuffer = ringBuffer;
    }

    public void run() {
        for (long i = 0; true; i++) {/*发布事件*/
            {
                long data = i;
                Print.tcfo("生产一个数据：" + data+ " | ringBuffer.remainingCapacity()= "+   ringBuffer.remainingCapacity());

                ringBuffer.publishEvent(TRANSLATOR, data);
                ThreadUtil.sleepSeconds(1);
            }
        }


    }
}