package com.crazymakercircle.objectpool;

public class CacheKey {
    public String key ;
    public CacheKey(String key){
        this.key = key;
    }
    @Override
    public int hashCode(){
        return (this.key).hashCode();
    }
}
