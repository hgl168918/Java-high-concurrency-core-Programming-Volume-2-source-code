package com.crazymakercircle.objectpool;

import com.crazymakercircle.util.Print;

import java.time.LocalTime;

public class CacheObject {
    LocalTime time;
    public String id;
    int seq = 0;
    
    public CacheObject(String id){
        this.id = id;
     }
    
    public void doing(){
        time = LocalTime.now();
        Print.tcfo(  "biz >>  using object "+id+ "  doing sth  " + seq++ +" at  time :"+    time  );
        this.hashCode();
    }
    
    public boolean valid(){
        Print.tcfo("biz >> 判断对象是否可用");
        return this.seq  < 2;
    }
}
