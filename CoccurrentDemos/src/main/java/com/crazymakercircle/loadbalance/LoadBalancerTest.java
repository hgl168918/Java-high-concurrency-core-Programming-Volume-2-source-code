package com.crazymakercircle.loadbalance;

import java.util.Arrays;

public class LoadBalancerTest {
    public static void main(String[] args) {
        // 创建服务器列表，服务器的权重分别为 5, 3, 2
        Server server1 = new Server("192.168.1.1", 5);
        Server server2 = new Server("192.168.1.2", 3);
        Server server3 = new Server("192.168.1.3", 2);
        
        WeightedRoundRobinBalancer balancer = new WeightedRoundRobinBalancer(
            Arrays.asList(server1, server2, server3)
        );
        
        // 模拟 20 次请求，观察每次请求分配到的服务器
        for (int i = 0; i < 20; i++) {
            Server selectedServer = balancer.doSelect();
            System.out.println(">>>>>>> 请求 " + (i + 1) + " 路由到: " + selectedServer.getIp());
        }

        System.out.println("=======》打印 总次数分布");

        Arrays.asList(server1, server2, server3).stream()
                .forEach(item->  System.out.println(item.getIp()+ ":" + item.getUsedCount()));


    }
}