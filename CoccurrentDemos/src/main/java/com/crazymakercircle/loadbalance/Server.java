package com.crazymakercircle.loadbalance;

import lombok.Data;

@Data
public class Server {
    private  int usedCount;   //使用次数
    private String ip;  //ip
    private int weight;  //权重
    private int currentWeight;  //当前权重
    
    public Server(String ip, int weight) {
        this.ip = ip;
        this.weight = weight;
        this.currentWeight = 0;
        this.usedCount = 0;
    }
    
    public void usedOne() {

        System.out.println("选中之后： ip = " + ip + " currentWeight = " + currentWeight + " weight = " + weight);
        usedCount++;
    }

}