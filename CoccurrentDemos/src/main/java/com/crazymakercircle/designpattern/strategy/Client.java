package com.crazymakercircle.designpattern.strategy;

import com.crazymakercircle.util.ThreadUtil;

public class Client {
    //持有上下文角色的引用
    private Context context;

    public Client(Context context) {
        this.context = context;
    }

    //向客户展示促销活动
    public void getPrice(){
        context.getStrategy().show();
    }


    public static void main(String[] args) {
        for (; ; ) {
            Context context=new Context();
            Client client = new Client(context);

            client.getPrice();
            ThreadUtil.sleepMilliSeconds(1000);
        }
    }
}