package com.crazymakercircle.generic.type;

import com.google.common.collect.Lists;
import org.junit.Test;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;

import static org.junit.Assert.*;

public class PlateDemo1Test {
    @Test
    public void upBoundDemo() {

        //定义一个水果盘子
        PlateDemo1<? extends Fruit> fruitPlate = null;

        //创建一个苹果盘子
        fruitPlate = new PlateDemo1<Apple>();

        //创建一个香蕉盘子
        fruitPlate = new PlateDemo1<Banana>();

        //创建一个放云南苹果盘子
        fruitPlate = new PlateDemo1<云南Apple>();

//        fruitPlate.set(new 云南Apple());
//        fruitPlate.set(new Banana());
//        fruitPlate.set(new Fruit());
//        fruitPlate.set(new Food());
//        fruitPlate.set(new Object());

        Fruit apple = fruitPlate.get();

    }


    @Test
    public void downBoundDemo() {

        //定义一个水果、或者食物 盘子
        PlateDemo1<? super Fruit> fruitPlate = null;
        //创建一个水果盘子
        fruitPlate = new PlateDemo1<Food>();
        //往盘子里加水果

        fruitPlate.set(new Apple());
        fruitPlate.set(new 云南Apple());
        fruitPlate.set(new Banana());
        fruitPlate.set(new Fruit());
//        fruitPlate.set(new Food());
//        fruitPlate.set(new Object());
//
//        Banana banana= fruitPlate.get();
//        Fruit fruit= fruitPlate.get();
//        Food apple= fruitPlate.get();
        Object object = fruitPlate.get();

    }


    @Test
    public void producerExtends() {

        //  声明消费者
        List<Fruit> consumer = Lists.newArrayList();

        //定义一个生产者，使用上界定义
        List<? extends Fruit> producer = Lists.newArrayList(new Banana(), new Apple());
        consumer.addAll(producer);

        for (Fruit fruit : producer) {
            System.out.println(fruit);
        }

    }

    @Test
    public void consumerSuper() {

        //  声明消费者
        List<? super Fruit> consumer = Lists.newArrayList();

        List<Fruit> producer = Lists.newArrayList(new Banana(), new Apple());
        consumer.addAll(producer);

        consumer.add(new Banana());
        consumer.add(new Apple());
        consumer.add(new 云南Apple());


    }

    @Test
    public void GenericTypeClear() {
        List<Fruit> fruits = Lists.newArrayList(new Banana(), new Apple());
        List<Food> foods = Lists.newArrayList(new Food());
        System.out.println("类型擦除 = " + (foods.getClass() == fruits.getClass()));
    }

    @Test
    public void unBoundTypeClear() {
        try {
            PlateDemo1<?> plateDemo = new PlateDemo1();
            Class<? extends PlateDemo1> cls = plateDemo.getClass();
            Field t = cls.getDeclaredField("someThing");
            System.out.println("someThing is :" + t.getType());
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        }


    }

    @Test
    public void upBoundTypeClear() {
        try {
            PlateDemo2<Apple> plateDemo = new PlateDemo2<>();
            Class<? extends PlateDemo2> cls = plateDemo.getClass();
            Field t = cls.getDeclaredField("someThing");
            System.out.println("someThing is :" + t.getType());
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        }


    }
    @Test
    public void downBoundTypeClear() {
        try {
            PlateDemo1<? super Fruit> plateDemo = new PlateDemo1();
            Class<? extends PlateDemo1> cls = plateDemo.getClass();
            Field t = cls.getDeclaredField("someThing");
            System.out.println("someThing is :" + t.getType());
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        }


    }

}


